FROM openjdk:11-jre-slim-buster
EXPOSE 8085
ARG JAR_FILE=target/offers-1.0.0.jar
ADD ${JAR_FILE} offers.jar
ENTRYPOINT ["java","-jar","/offers.jar"]