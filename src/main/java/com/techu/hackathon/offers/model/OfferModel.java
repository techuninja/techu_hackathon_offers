package com.techu.hackathon.offers.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "offers")
public class OfferModel {

    @Id
    @NotNull
    private String id;
    @NotNull
    private String offerCode;
    @NotNull
    private String nombre;
    private String titulo;
    private String descripcion;
    private String urlBanner;
    private String landingUrl;
    private List<FlagsModel> flags;

    public OfferModel(@NotNull String id,String offerCode,String nombre,String titulo,String descripcion,String urlBanner,String landingUrl,List<FlagsModel> flags){
        this.setId(id);
        this.setOfferCode(offerCode);
        this.setNombre(nombre);
        this.setTitulo(titulo);
        this.setDescripcion(descripcion);
        this.setUrlBanner(urlBanner);
        this.setLandingUrl(landingUrl);
        this.setFlags(flags);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOfferCode() {
        return offerCode;
    }

    public void setOfferCode(String offerCode) {
        this.offerCode = offerCode;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrlBanner() {
        return urlBanner;
    }

    public void setUrlBanner(String urlBanner) {
        this.urlBanner = urlBanner;
    }

    public String getLandingUrl() {
        return landingUrl;
    }

    public void setLandingUrl(String landingUrl) {
        this.landingUrl = landingUrl;
    }

    public List<FlagsModel> getFlags() {
        return flags;
    }

    public void setFlags(List<FlagsModel> flags) {
        this.flags = flags;
    }
}
