package com.techu.hackathon.offers.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "flags")
public class FlagsModel {

    private String flagCode;
    private String valueRequired;

    public FlagsModel(String flagCode, String valueRequired){
        this.setFlagCode(flagCode);
        this.setValueRequired(valueRequired);
    }

    public String getFlagCode() {
        return flagCode;
    }

    public void setFlagCode(String flagCode) {
        this.flagCode = flagCode;
    }

    public String getValueRequired() {
        return valueRequired;
    }

    public void setValueRequired(String valueRequired) {
        this.valueRequired = valueRequired;
    }
}
