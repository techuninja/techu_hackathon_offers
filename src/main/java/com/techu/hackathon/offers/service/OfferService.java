package com.techu.hackathon.offers.service;

import com.techu.hackathon.offers.model.OfferModel;

import java.util.List;
import java.util.Map;

public interface OfferService {


    List<OfferModel> findOffersByFlags(Map<String, String> listadoFlags);

    List<OfferModel> findAll();
    OfferModel findByCode(String offerCode);
    OfferModel nuevaOferta(OfferModel oferta);
    void eliminarOferta(String offerCode);
}
