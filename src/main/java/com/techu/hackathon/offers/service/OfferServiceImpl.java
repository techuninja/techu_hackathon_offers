package com.techu.hackathon.offers.service;

import com.techu.hackathon.offers.model.FlagsModel;
import com.techu.hackathon.offers.model.OfferModel;
import com.techu.hackathon.offers.repository.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("offerService")
@Transactional
public class OfferServiceImpl implements OfferService {
    OfferRepository offerRepository;

    @Autowired
    public OfferServiceImpl(OfferRepository offerRepository){this.offerRepository = offerRepository;}

    @Override
    public List<OfferModel> findOffersByFlags(Map<String, String> listadoFlags) {
        List<OfferModel> allOffers= this.findAll();
        List<OfferModel> ofertasValidas=new ArrayList<>();


        for (OfferModel oferta: allOffers) {
            System.out.println("Validando la Oferta: "+ oferta.getNombre());
            Boolean aplicaOferta=false;

            for (FlagsModel flagOferta:oferta.getFlags() ) {
                System.out.println("Validando el Flag de Oferta: "+ flagOferta.getFlagCode()+":"+flagOferta.getValueRequired());
                String value=listadoFlags.get(flagOferta.getFlagCode());
                if(value!=null){
                    if(value.equals(flagOferta.getValueRequired())){
                        aplicaOferta=true;
                    }else{
                        aplicaOferta=false;
                        break;
                    }
                }else{
                    System.out.println("FLag no encontrado: "+flagOferta.getFlagCode());
                    break;
                }

            }
            if(aplicaOferta){
                System.out.println("Coincidencia encontrada, se agrega oferta a lista");
                ofertasValidas.add(oferta);
            }
        }

//        for (Map.Entry<String, String> flag : listadoFlags.entrySet()) {
//            System.out.println("=====================");
//            System.out.println("Voy a Validar Flag:"+flag.getKey()+":"+flag.getValue());
//            for (OfferModel oferta: allOffers) {
//                System.out.println("Validando la Oferta: "+ oferta.getNombre());
//                for (FlagsModel flagOferta:oferta.getFlags() ) {
//                    System.out.println("Validando el Flag de Oferta: "+ flagOferta.getFlagCode()+":"+flagOferta.getValueRequired());
//                    if(flagOferta.getFlagCode().equals(flag.getKey()) && flagOferta.getValueRequired().equals(flag.getValue())){
//                        System.out.println("Coincidencia encontrada, se agrega oferta a lista");
//                        ofertasValidas.add(oferta);
//                        break;
//                    }
//
//                }
//            }
//        }

        System.out.println("Ofertas q aplican: "+ofertasValidas.toString());
        
        return ofertasValidas;
    }



    @Override
    public List<OfferModel> findAll() {
        return offerRepository.findAll();
    }

    @Override
    public OfferModel findByCode(String offerCode) {
        return offerRepository.findByCode(offerCode);
    }

    @Override
    public OfferModel nuevaOferta(OfferModel oferta) {
        return offerRepository.nuevaOferta(oferta);
    }

    @Override
    public void eliminarOferta(String offerCode) {
        offerRepository.eliminarOferta(offerCode);
    }
}

