package com.techu.hackathon.offers.controller;


import com.techu.hackathon.offers.model.OfferModel;
import com.techu.hackathon.offers.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/offers")
public class OfferController {
   // private static Map<String, String> listaFlags = new HashMap<>();

    @Autowired
    OfferService offerService;


    @PostMapping(value = "/flags")
    public ResponseEntity<Object> obtenerOfertasByFlags(@RequestBody Map<String, String> listadoFlags)
    {
        List<OfferModel> listaOfertas = offerService.findOffersByFlags(listadoFlags);
        if(listaOfertas!= null){
            return ResponseEntity.ok(listaOfertas);
        }else{
            return new ResponseEntity<>(String.format("No tiene Ofertas"),HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping()
    public ResponseEntity<List<OfferModel>> obtenerOfertas() {
        System.out.println("Me piden la lista de ofertas");
        return ResponseEntity.ok(offerService.findAll());
    }

    @GetMapping(value = "/{offerCode}")
    public ResponseEntity<OfferModel> obtenerCuentas(@PathVariable("offerCode") String offerCode) {
        return ResponseEntity.ok(offerService.findByCode(offerCode));
    }


    @PostMapping()
    public ResponseEntity<OfferModel> nuevaCuenta(@RequestBody OfferModel oferta)
    {
        return ResponseEntity.ok(offerService.nuevaOferta(oferta));
    }

    @DeleteMapping(value = "/{offerCode}")
    public ResponseEntity<Object> eliminarCuenta(@PathVariable("offerCode") String offerCode){
        if(offerService.findByCode(offerCode)!=null){
            offerService.eliminarOferta(offerCode);
            return new ResponseEntity<>(String.format("Oferta %s eliminada", offerCode), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(String.format("Oferta %s no existe",offerCode),HttpStatus.NOT_FOUND);
        }
    }

}
