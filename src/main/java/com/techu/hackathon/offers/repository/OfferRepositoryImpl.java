package com.techu.hackathon.offers.repository;

import com.techu.hackathon.offers.model.OfferModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class OfferRepositoryImpl implements OfferRepository{
    private final MongoOperations mongoOperations;

    @Autowired
    public OfferRepositoryImpl(MongoOperations mongoOperations){this.mongoOperations=mongoOperations;}

    @Override
    public List<OfferModel> findAll() {
        List<OfferModel> ofertas = this.mongoOperations.find(new Query(), OfferModel.class);
        System.out.println("ofertas:"+ofertas.toString());
        return ofertas;
    }

    @Override
    public OfferModel findByCode(String offerCode) {
        OfferModel encontrado = this.mongoOperations.findOne(new Query(Criteria.where("nroCuenta").is(offerCode)),OfferModel.class);
        return encontrado;
    }

    @Override
    public OfferModel nuevaOferta(OfferModel oferta) {
        this.mongoOperations.save(oferta);
        return findByCode(oferta.getOfferCode());
    }

    @Override
    public void eliminarOferta(String offerCode) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("offerCode").is(offerCode)),OfferModel.class);
    }
}
