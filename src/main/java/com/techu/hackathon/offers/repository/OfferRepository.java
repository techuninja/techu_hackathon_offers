package com.techu.hackathon.offers.repository;

import com.techu.hackathon.offers.model.OfferModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferRepository {

    List<OfferModel> findAll();
    OfferModel findByCode(String offerCode);
    OfferModel nuevaOferta(OfferModel oferta);
    void eliminarOferta(String offerCode);
}
